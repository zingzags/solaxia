/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.server.channel.handlers;

import client.MapleClient;
import client.autoban.AutobanFactory;
import net.AbstractMaplePacketHandler;
import tools.data.input.SeekableLittleEndianAccessor;

/**
 *
 * @author Tyler
 */
public class AutobanHandler extends AbstractMaplePacketHandler {
    @Override
    public void handlePacket(SeekableLittleEndianAccessor slea, MapleClient c) {
        int type = slea.readByte();

        switch (type) {
            case 2:
                c.getPlayer().getAutobanManager().addPoint(AutobanFactory.ITEM_VAC, "Item Vac");
                break;
            default:
                AutobanFactory.GENERAL.alert(c.getPlayer(), "Player suspected of hacking. Reason ID: " + type);
           
                break;
        }
    }
}
