/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import java.util.HashMap;
import java.util.Map;

import provider.MapleData;
import provider.MapleDataTool;
import server.MapleItemInformationProvider;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;
import client.MapleCharacter;
import client.inventory.Item;
import client.inventory.MapleInventoryType;

/**
 *
 * @author Tyler
 */
public class ItemRequirement extends MapleQuestRequirement {
	Map<Integer, Integer> items = new HashMap<>();
	
	
	public ItemRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.ITEM);
		processData(data);
	}
	
	@Override
	public void processData(MapleData data) {
		for (MapleData itemEntry : data.getChildren()) {
			int itemId = MapleDataTool.getInt(itemEntry.getChildByPath("id"));
			int count = MapleDataTool.getInt(itemEntry.getChildByPath("count"), 0);
			
			items.put(itemId, count);
		}
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
		for(Integer itemId : items.keySet()) {
			int countNeeded = items.get(itemId);
			int count = 0;
			
			MapleInventoryType iType = ii.getInventoryType(itemId);
			
			if (iType.equals(MapleInventoryType.UNDEFINED)) {
				return false;
			}
			for (Item item : chr.getInventory(iType).listById(itemId)) {
				count += item.getQuantity();
			}
			//Weird stuff, nexon made some quests only available when wearing gm clothes. This enables us to accept it ><
			if (iType.equals(MapleInventoryType.EQUIP)) {
				for (Item item : chr.getInventory(MapleInventoryType.EQUIPPED).listById(itemId)) {
					count += item.getQuantity();
				}
			}
			
			if(count < countNeeded || countNeeded <= 0 && count > 0) {
				return false;
			}
		}
		return true;
	}
	
	public int getItemAmountNeeded(int itemid) {
		if(items.containsKey(itemid)) {
			return items.get(itemid);
		}
		
		return 0;
	}
}
