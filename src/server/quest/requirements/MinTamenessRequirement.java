/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import client.MapleCharacter;
import client.inventory.MaplePet;
import provider.MapleData;
import provider.MapleDataTool;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;

/**
 *
 * @author Tyler
 */
public class MinTamenessRequirement extends MapleQuestRequirement {
	private int minTameness;
	
	
	public MinTamenessRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.MIN_PET_TAMENESS);
		processData(data);
	}
	
	/**
	 * 
	 * @param data 
	 */
	@Override
	public void processData(MapleData data) {
		minTameness = MapleDataTool.getInt(data);
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		int curCloseness = 0;
		for(MaplePet pet : chr.getPets()) {
			if(pet.getCloseness() > curCloseness)
				curCloseness = pet.getCloseness();
		}
		
		return curCloseness >= minTameness;
	}
}
