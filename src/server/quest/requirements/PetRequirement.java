/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import client.MapleCharacter;
import client.inventory.MaplePet;
import java.util.ArrayList;
import java.util.List;
import provider.MapleData;
import provider.MapleDataTool;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;

/**
 *
 * @author Tyler
 */
public class PetRequirement extends MapleQuestRequirement {
	List<Integer> petIDs = new ArrayList<>();
	
	
	public PetRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.PET);
		processData(data);
	}
	
	
	@Override
	public void processData(MapleData data) {
		for(MapleData petData : data.getChildren()) {
			petIDs.add(MapleDataTool.getInt(petData.getChildByPath("id")));
		}
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		for(MaplePet pet : chr.getPets()) {
			if(petIDs.contains(pet.getItemId()))
				return true;
		}
		
		return false;
	}
}
