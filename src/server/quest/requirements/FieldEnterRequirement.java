/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import client.MapleCharacter;
import provider.MapleData;
import provider.MapleDataTool;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;

/**
 *
 * @author Tyler
 */
public class FieldEnterRequirement extends MapleQuestRequirement {
	private int mapId = -1;
	
	
	public FieldEnterRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.FIELD_ENTER);
		processData(data);
	}
	
	@Override
	public void processData(MapleData data) {
		MapleData zeroField = data.getChildByPath("0");
		if (zeroField != null) {
			 mapId = MapleDataTool.getInt(zeroField);
		}
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		return mapId == chr.getMapId();
	}
}
