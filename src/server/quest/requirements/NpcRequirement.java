/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import client.MapleCharacter;
import provider.MapleData;
import provider.MapleDataTool;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;

/**
 *
 * @author Tyler
 */
public class NpcRequirement extends MapleQuestRequirement {
	private int reqNPC;
	private final boolean autoComplete, autoStart;
	
	public NpcRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.NPC);
		processData(data);
		this.autoComplete = quest.isAutoComplete();
		this.autoStart = quest.isAutoStart();
	}
	
	@Override
	public void processData(MapleData data) {
		reqNPC = MapleDataTool.getInt(data);
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		return npcid != null && npcid == reqNPC && (autoComplete || autoStart || chr.getMap().containsNPC(npcid));
	}
}
