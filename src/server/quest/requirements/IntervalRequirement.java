/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import client.MapleCharacter;
import client.MapleQuestStatus;
import provider.MapleData;
import provider.MapleDataTool;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;

/**
 *
 * @author Tyler
 */
public class IntervalRequirement extends MapleQuestRequirement {
	private int interval = -1;
	private int questID;
	
	public IntervalRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.INTERVAL);
		processData(data);
		questID = quest.getId();
	}
	
	
	@Override
	public void processData(MapleData data) {
		interval = MapleDataTool.getInt(data) * 60 * 1000;
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		boolean check = !chr.getQuest(MapleQuest.getInstance(questID)).getStatus().equals(MapleQuestStatus.Status.COMPLETED);
		boolean check2 = chr.getQuest(MapleQuest.getInstance(questID)).getCompletionTime() <= System.currentTimeMillis() - interval;
		return check || check2;
	}
}
