/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import java.util.ArrayList;
import java.util.List;

import provider.MapleData;
import provider.MapleDataTool;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;
import client.MapleCharacter;
import client.MapleJob;

/**
 *
 * @author Tyler
 */
public class JobRequirement extends MapleQuestRequirement {
	List<Integer> jobs = new ArrayList<>();
	
	public JobRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.JOB);
		processData(data);
	}
	
	/**
	 * 
	 * @param data 
	 */
	@Override
	public void processData(MapleData data) {
		for (MapleData jobEntry : data.getChildren()) {
			jobs.add(MapleDataTool.getInt(jobEntry));
		}
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		for(Integer job : jobs) {
			if (chr.getJob().equals(MapleJob.getById(job)) || chr.isGM()) {
				return true;
			}
		}
		return false;
	}
}
