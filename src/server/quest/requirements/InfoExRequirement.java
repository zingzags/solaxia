/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.quest.requirements;

import client.MapleCharacter;
import client.MapleQuestStatus;
import java.util.ArrayList;
import java.util.List;
import provider.MapleData;
import provider.MapleDataTool;
import server.quest.MapleQuest;
import server.quest.MapleQuestRequirementType;

/**
 *
 * @author Tyler
 */
public class InfoExRequirement extends MapleQuestRequirement {
	private List<String> infoExpected = new ArrayList<>();
	private int questID;
	
	
	public InfoExRequirement(MapleQuest quest, MapleData data) {
		super(MapleQuestRequirementType.INFO_EX);
		processData(data);
		questID = quest.getId();
	}
	
	@Override
	public void processData(MapleData data) {
		// Because we have to...
		for(MapleData infoEx : data.getChildren()) {
			MapleData value = infoEx.getChildByPath("value");
			infoExpected.add(MapleDataTool.getString(value, ""));
		}
	}
	
	
	@Override
	public boolean check(MapleCharacter chr, Integer npcid) {
		MapleQuestStatus status = chr.getQuest(MapleQuest.getInstance(questID));
		return infoExpected.contains(status.getInfo());
	}
	
	public List<String> getInfo() {
		return infoExpected;
	}
	
	public String getFirstInfo() {
		return !infoExpected.isEmpty() ? infoExpected.get(0) : "";
	}
}
