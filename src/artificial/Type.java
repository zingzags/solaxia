package artificial;

/**
 *
 * @author Brent (Zygon)
 */
public enum Type {
    
    // Support companions
    
    /**
     * Alchemists are able to transmute matter into different forms and
     * are experts on transmogrification.  They tend to be troublemakers at
     * heart; but, they are very reliable due to their vast amount of knowledge
     * and utility.
     * 
     * Name: Jared (M) / Rose (F)
     * 
     * Hometown: Magatia
     * 
     * Utility: Creation of potions and other useful items through dialogue.
     * 
     * Open World: Alchemist companions can randomly morph other players while
     * in a town and within a certain distance for a set amount of time.
     * 
     * PvE: 
     *      1st - Transmogrify: Can transform monsters into a weaker monster
     *                          that has the same base experience and health.  
     *                          Does not work on bosses or monsters above 10 
     *                          levels of the partner.
     * 
     *      2nd - Connection:   An alchemist is able to transform dead monsters
     *                          into vitality and magic to recover their partner
     *                          in the heat of battle.
     * 
     *      3rd - Transmute:    An alchemist can create rare items from raw 
     *                          materials such as money and common items.
     * 
     *      4th - Breath Pact:  When the partner of an alchemist falls, he is able
     *                          to call upon a forbidden pact to bring life back
     *                          to their partner at the expense of their vitality
     *                          for a set duration.
     */
    ALCHEMIST,
    
    /**
     * 
     */
    AURORA,
    
    /**
     * Ninja lurk in the shadows awaiting the opportunity to strike
     * the killing blow onto their enemies.  Masters of concealment,
     * they practice various martial arts to keep their body in peak condition.
     * Despite their quiet nature, they are very loyal to the individuals they
     * choose as partners.
     * 
     * Name: Roho (M) / Alma (F)
     *
     * Hometown: Showa (?)
     *
     * Utility: Ninja can be used in strategic encounters where avoiding damage
     * is critical to survival.  They can also be used to help exterminate
     * weaker enemies in one fell swoop.
     *
     * Open World:
     *
     * PvE:
     *      1st - Assault:      Concealing themselves in the shadows, ninja
     *                          strike at the moment their opponent becomes
     *                          vulnerable to a finishing blow.
     *
     *      2nd - Kunai Mark:   Ninja mark their enemies with kunai that are
     *                          dressed in poison, paralysis, confusion, and
     *                          freeze serums. Their partners can amplify their
     *                          damage by attacking afflicted targets.
     *
     *      3rd - Smokescreen:  Tools of the trade are important to know how to
     *                          use.  Setting up a good smokescreen can help
     *                          keep their partner safe.
     *
     *      4th - Blood Bond:   Ninja are bonded with their partner completely,
     *                          empowering their and their partner's ability to
     *                          spot weakness in their enemies flawlessly.
     */
    NINJA,
    
    /**
     * Guardians are the ultimate front line warriors.  They can take any
     * beating, climb any mountain, and conquer any foe.  Although they are 
     * generally very serious due to their extensive combat histories, they
     * are pure at heart and wish for a world of peace.
     * 
     * Name:
     * 
     * Hometown: Orbis
     * 
     * Utility:  Guardians serve as a psuedo-tank for partners that tend to be
     * very squishy and cannot take serious punishment like their tanky brethren
     * can.  Guardians strike fear into their foes and are able to attract
     * monsters through manipulating their aura.
     * 
     * 
     * 
     */
    GUARDIAN,
    ;
    
    //private int id;
}
