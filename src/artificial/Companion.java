package artificial;

import client.MapleCharacter;
import client.MapleClient;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import net.SendOpcode;
import server.life.MapleMonster;
import server.maps.AbstractAnimatedMapleMapObject;
import server.maps.MapleFoothold;
import server.maps.MapleMap;
import server.maps.MapleMapObject;
import server.maps.MapleMapObjectType;
import server.movement.AbsoluteLifeMovement;
import server.movement.JumpDownMovement;
import server.movement.LifeMovementFragment;
import server.movement.RelativeLifeMovement;
import server.movement.TeleportMovement;
import tools.MaplePacketCreator;
import tools.Pair;
import tools.data.output.MaplePacketLittleEndianWriter;

/**
 * Represents an artificial player (bot) that can be on a map
 * and can interact with other elements on the same map.
 * 
 * Ideally, an artificial player will have comparable intelligence to a
 * regular player and can perform various tasks based upon the artificial
 * player's job specification.
 * 
 * @author Brent (Zygon)
 */
public class Companion extends AbstractAnimatedMapleMapObject {

    private int id;
    //private int linkedId; // for future use, when looks are loaded from sql (?)
    private MapleMap map;
    private boolean stationed;
    private MapleCharacter owner;
    private final Rectangle rRange;
    //private ReentrantLock lock = new ReentrantLock(true);
    // character aesthetics
    private int face;
    private int hair;
    private int gender;
    private int skinColor;
    private String name;
    private final HashMap<Byte, Integer> eqps;
    private static AtomicInteger runningId = new AtomicInteger(1000);
    public static final int PIXEL_DISTANCE_X = 75;

    public Companion(int g) {
        this(g, runningId.getAndIncrement());
    }

    public Companion(int g, int sid) {
        id = sid;
        eqps = new HashMap<>();
        gender = g;
        rRange = new Rectangle(1000, 200); // not sure, but meh
        applyGenericBotLooks();
    }

    public boolean isStationed() {
        return stationed;
    }

    public void toggleStationed() {
        stationed = !stationed;
        if (stationed) {
            // XXX start patrol thread...
        }
    }

    public void setOwner(MapleCharacter mc) {
        owner = mc;
    }

    public MapleCharacter getOwner() {
        return owner;
    }

    private void applyGenericBotLooks() {
        eqps.put((byte) -11, 1492000); // pistol
        eqps.put((byte) -103, 1022004); // glasses
        eqps.put((byte) -111, 1702158); // cover gun
        if (gender == 0) {
            face = 20005;
            hair = 30460;
            name = "Mafioso";
            eqps.put((byte) -105, 1050113); // tuxedo
            eqps.put((byte) -107, 1072010); // dress shoes
            eqps.put((byte) -108, 1082231); // wristwatch
        } else {
            face = 21001;
            hair = 31020;
            name = "Mafiosa";
            eqps.put((byte) -105, 1051163); // gothic dress
            eqps.put((byte) -107, 1071021); // gothic shoes
        }
    }

    // for attacking monsters
    public final void checkRange() {
        Collection<MapleMapObject> objs = map.getMapObjects();
        LinkedList<MapleMonster> left = new LinkedList<>();
        LinkedList<MapleMonster> right = new LinkedList<>();
        for (MapleMapObject o : objs) {
            if (o.getType().equals(MapleMapObjectType.MONSTER)) {
                int rBox = inRange(o.getPosition());
                if (rBox == 1) {
                    left.add((MapleMonster) o);
                } else if (rBox == 2) {
                    right.add((MapleMonster) o);
                }
            }
        }
        if (left.size() == 0 && right.size() == 0) {
            return;
        }
        int skill, stance, unk80 = 0x57, count;
        boolean lprior = left.size() >= right.size();
        List<Pair<Integer, List<Integer>>> damage = new LinkedList<>();
        if (lprior) {
            stance = 0x80;
//            if (left.size() > 1) {
//                skill = 5201001;
//                count = 1 + (left.size() * 16);
//                for (MapleMonster target : left) {
//                    damage.add(new Pair(target.getObjectId(), Collections.singletonList(5000)));
//                }
//            } else {
                skill = 5210000;               
                count = 0x13;
                LinkedList<Integer> toApply = new LinkedList<>();
                for (int i = 0; i < 3; i++) {
                    toApply.add(5000); // randomize damage in future
                }
                damage.add(new Pair<Integer, List<Integer>>(left.pop().getObjectId(), toApply));
//            }       
        } else {
            stance = 0x00;
//            if (right.size() > 1) {
//                skill = 5201001;
//                count = 1 + (right.size() * 16);
//                for (MapleMonster target : right) {
//                    damage.add(new Pair(target.getObjectId(), Collections.singletonList(5000)));
//                }
//            } else {
                skill = 5210000;
                count = 0x13;                
                LinkedList<Integer> toApply = new LinkedList<>();
                for (int i = 0; i < 3; i++) {
                    toApply.add(5000); // randomize damage in future
                }
                toApply.add(0x00);
                damage.add(new Pair<Integer, List<Integer>>(right.pop().getObjectId(), toApply));
//            }
        }
        for (Pair<Integer, List<Integer>> hit : damage) {
            int total = 0;
            for (Integer i : hit.getRight()) {
                total += i;
            }
            MapleMonster m = map.getMonsterByOid(hit.getLeft());
            if (m != null) // bad synchro probably
                map.damageMonster(owner, m, total);
        }
//        MaplePacket p = MaplePacketCreator.rangedAttack(id, skill, stance, count, 2330005, damage, 0x03, (byte) unk80);
//        System.out.printf("Dump: %s%n", p.toString());
//        map.broadcastMessage(p);
        // map.broadcastMessage(MaplePacketCreator.rangedAttack(id, id, face, linkedId, face, null, id, UNK80));
    }

    private int inRange(Point target) {
        int t_x = target.x;
        int t_y = target.y;
        int x = getPosition().x;
        int y = getPosition().y;
        synchronized (rRange) {
            if (x != rRange.x || y != rRange.y) {
                rRange.x = x - (rRange.width / 2);
                rRange.y = y - rRange.height; // not sure how this works, but meh
            }
        }
        return rRange.contains(t_x, t_y) ? x >= t_x ? 1 : 2 : 0;
    }

    public synchronized final void calculateFollowMovement(Collection<LifeMovementFragment> mov, int placement) {
        if (stationed) {
            return;
        }
//        lock.lock();
        try {
            List<LifeMovementFragment> aiMov = new LinkedList<>();
            int X_OFFSET = placement * PIXEL_DISTANCE_X;
            for (LifeMovementFragment mf : mov) {
                Point aiPos = getPosition();
                if (mf instanceof AbsoluteLifeMovement) {
                    AbsoluteLifeMovement alm = (AbsoluteLifeMovement) mf;
                    Point nP = new Point(0, aiPos.y);
                    Point botPPS = new Point(0, 0);
                    Point pps = alm.getPixelsPerSecond();
                    Point pos = mf.getPosition();
                    int dur = alm.getDuration();
                    int ns = alm.getNewstate();
                    int xDist = Math.abs(pos.x - aiPos.x);
                    boolean noChangeX = false;
                    if (xDist > X_OFFSET) {
                        if (pps.x != 0) {
                            botPPS.x = pps.x;
                        }
                        if (pos.x > aiPos.x) {
                            nP.x = pos.x - X_OFFSET;
                        } else {
                            nP.x = pos.x + X_OFFSET;
                        }
                        MapleFoothold fh = map.getFootholds().findBelow(nP);
                        if (fh != null) {
                            nP.y = fh.calculateFooting(nP.x);
                        }
                        boolean teleport = mf instanceof TeleportMovement || mf instanceof RelativeLifeMovement;
                        if (teleport) {
                            TeleportMovement tele = new TeleportMovement(4, nP, 0);
                            tele.setPixelsPerSecond(new Point(0, owner.isFacingLeft() ? 3 : 2)); // meh
                            tele.setUnk(0);
                            aiMov.add(tele);
                        }
                        MapleFoothold cur = map.getFootholds().findBelow(owner.getPosition());
                        if (cur == null) {
                            continue; // not sure about this, but always can come back
                        }
                        
                        // XXX jump movement can be done instead
                        if (cur.getY1() != getPosition().y) {
                            teleport = true;
                            nP.x = owner.getPosition().x;
                            nP.y = cur.calculateFooting(nP.x); // may or may not work, let's find out
                            TeleportMovement tele = new TeleportMovement(4, nP, 0);
                            tele.setPixelsPerSecond(new Point(0, owner.isFacingLeft() ? 3 : 2));
                            tele.setUnk(0);
                            aiMov.add(tele);
                        }
                        if (teleport) {
                            setPosition(nP);
                            continue;
                        }
                    } else {
                        noChangeX = true;
                        nP.x = aiPos.x;
                        ns = pos.x < aiPos.x ? 5 : 4;
                    }
                    if (ns < 2 || ns > 5) {
                        ns = owner.isFacingLeft() ? (noChangeX ? 5 : 3) : (noChangeX ? 4 : 2);
                    }
                    AbsoluteLifeMovement bm = new AbsoluteLifeMovement(0, nP, dur, ns); // hmm idk
                    bm.setPixelsPerSecond(botPPS);
                    bm.setUnk(alm.getUnk());
                    aiMov.add(bm);
                    setPosition(nP);
                } else if (mf instanceof JumpDownMovement) { // may adjust this later
                    sendSpawnData(owner.getClient());
                }
            }
            if (!aiMov.isEmpty()) {
                map.broadcastMessage(MaplePacketCreator.movePlayer(id, aiMov));
            }
//            checkRange(); // XXX Make call to this when a monster moves also
        } catch (Exception e) {
            e.printStackTrace();
        }
//        finally {
//            lock.unlock();
//        }
    }

    public void showFallDown() {
        Point aiPos = getPosition();
        List<LifeMovementFragment> aiMov = new LinkedList<>();
        AbsoluteLifeMovement bm1 = new AbsoluteLifeMovement(0, getPosition(), 300, 6);
        bm1.setPixelsPerSecond(new Point(0, 600)); // positive y is down, don't ask
        bm1.setUnk(0); // idk
        aiMov.add(bm1);
        MapleFoothold fh = map.getFootholds().findBelow(getPosition());
        int y1 = fh.getY1(), y2 = fh.getY2();
        int useY = y1;
        if (y1 < y2 || y1 > y2) {
            // XXX calculate y drop closest to where we want to be if for some reason
            // there is a slope spawn position
        }
        aiPos.y = useY;
        AbsoluteLifeMovement bm2 = new AbsoluteLifeMovement(0, new Point(aiPos.x, useY), 12, 6);
        bm2.setPixelsPerSecond(new Point(0, 0));
        bm2.setUnk(0);
        aiMov.add(bm2);
        AbsoluteLifeMovement bm3 = new AbsoluteLifeMovement(0, new Point(aiPos.x, useY), 198, 4);
        bm3.setPixelsPerSecond(new Point(0, 0));
        bm3.setUnk(0);
        aiMov.add(bm3);
        map.broadcastMessage(MaplePacketCreator.movePlayer(id, aiMov));
    }

    public MapleMap getMap() {
        return map;
    }

    public void setMap(MapleMap m) {
        map = m;
    }

    public int getSpecialId() {
        return id;
    }

    public final String getName() {
        return name;
    }

    public int getFace() {
        return face;
    }

    public int getHair() {
        return hair;
    }

    public int getGender() {
        return gender;
    }

    public int getSkinColor() {
        return skinColor;
    }

    public Map<Byte, Integer> getEquips() {
        return eqps;
    }

    @Override
    public MapleMapObjectType getType() {
        return MapleMapObjectType.PLAYER;
    }

    @Override
    public void sendSpawnData(MapleClient c) {
    	c.announce(spawnCompanion());
    }

    @Override
    public void sendDestroyData(MapleClient c) {
    	c.announce(MaplePacketCreator.removePlayerFromMap(id));
    }

    public final void spawn(boolean fresh) {
        setPosition(owner.getPosition()); // meh...
        map.addCompanion(this, fresh);
        showFallDown();
    }
    
    private byte[] spawnCompanion() {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendOpcode.SPAWN_PLAYER.getValue());
        mplew.writeInt(id);
        mplew.write(200);
        mplew.writeMapleAsciiString(name);
        mplew.write(new byte[8]);
        mplew.writeInt(0x00);
        mplew.writeShort(0);
        mplew.write(0xFC);
        mplew.write(1);
        mplew.writeInt(0);
        mplew.writeInt(0x00);
        mplew.writeInt(0x00);
        int CHAR_MAGIC_SPAWN = new Random().nextInt();
        mplew.writeInt(0);
        mplew.writeShort(0);
        mplew.writeInt(CHAR_MAGIC_SPAWN);
        mplew.writeShort(0); //v74
        mplew.write(0); //v74
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(CHAR_MAGIC_SPAWN);//v74
        mplew.writeShort(0);
        mplew.write(0); //v74
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(CHAR_MAGIC_SPAWN);
        mplew.writeShort(0);
        mplew.write(0);
        mplew.writeLong(0);
        mplew.writeInt(CHAR_MAGIC_SPAWN);
        mplew.write(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(CHAR_MAGIC_SPAWN);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(CHAR_MAGIC_SPAWN);
        mplew.write(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.writeInt(CHAR_MAGIC_SPAWN);
        mplew.writeShort(0);
        mplew.write(0);
        mplew.writeShort(900); // XXX GM? Not sure...
        addCompanionLooks(mplew);
        mplew.writeInt(0x00);
        mplew.writeInt(-1);
        mplew.writeInt(0x00);
        mplew.writeShort(getPosition().x);
        mplew.writeShort(getPosition().y);
        mplew.write(getStance());
        mplew.writeShort(0);
        mplew.write(0);
        mplew.write(0); // End Of Pets
        mplew.writeInt(1); // Mob Level
        mplew.writeLong(0); // Mob Experience + Tiredness
        mplew.write(0x00);
        mplew.write(0x00);
        mplew.write(0x00);
        mplew.write(0x00);
        mplew.writeInt(0);
        return mplew.getPacket();
    } 
    
    private void addCompanionLooks(MaplePacketLittleEndianWriter mplew) {
        mplew.write(gender);
        mplew.write(skinColor);
        mplew.writeInt(face);
        mplew.write(0x01);
        mplew.writeInt(hair);
        Map<Byte, Integer> equip = getEquips();
        Map<Byte, Integer> myEquip = new LinkedHashMap<>();
        Map<Byte, Integer> maskedEquip = new LinkedHashMap<>();
        for (Entry<Byte, Integer> item : equip.entrySet()) {
            byte pos = (byte) -item.getKey();
            if (pos < 100 && myEquip.get(pos) == null) {
                myEquip.put(pos, item.getValue());
            } else if (pos > 100 && pos != 111) { // don't ask. o.o
                pos -= 100;
                if (myEquip.get(pos) != null) {
                    maskedEquip.put(pos, myEquip.get(pos));
                }
                myEquip.put(pos, item.getValue());
            } else if (myEquip.get(pos) != null) {
                maskedEquip.put(pos, item.getValue());
            }
        }
        for (Entry<Byte, Integer> entry : myEquip.entrySet()) {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }
        mplew.write(0xFF); // end of visible itens
        for (Entry<Byte, Integer> entry : maskedEquip.entrySet()) {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }
        mplew.write(0xFF);
        mplew.writeInt(equip.containsKey((byte) -111) ? equip.get((byte) -111) : 0);
        for (int i = 0; i < 3; i++) {
            mplew.writeInt(0);
        }
    }

}
